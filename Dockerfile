FROM anapsix/alpine-java:8u202b08_jdk

MAINTAINER rs-team@realsee.net

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

RUN mkdir -p /app

WORKDIR /app

EXPOSE 8001

ADD ./target/wcs-admin.jar ./app.jar

CMD java -Xmx256m -Xms256m -Xss1024k -XX:+UseG1GC -XX:+DisableExplicitGC -jar app.jar

FROM openjdk:11
# FROM: 基础镜像，基于jdk8镜像开始
COPY *.jar /app.jar
# COPY: 将应用的配置文件也拷贝到镜像中。
CMD ["--server.port=8080"]
EXPOSE 8080
# EXPOSE：声明端口ENTRYPOINT ["java","-jar","/app.jar"]
# ENTRYPOINT：docker启动时，运行的命令，这里容器启动时直接运行jar服务。