package com.laoayu.parking.system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson2.JSONObject;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.enums.scope.AuthGiteeScope;
import me.zhyd.oauth.enums.scope.AuthWeiboScope;
import me.zhyd.oauth.exception.AuthException;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthToken;
import me.zhyd.oauth.request.AuthGiteeRequest;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.request.AuthWeiboRequest;
import me.zhyd.oauth.utils.AuthScopeUtils;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;


@CrossOrigin
@Configuration
@RestController
@RequestMapping("/oauth")
public class RestAuthController {

    //    http://localhost/dev-api/system/oauth/render/gitee
    @RequestMapping("/render/{source}")
    public void renderAuth(@PathVariable("source") String source, HttpServletResponse response) throws IOException {
        AuthRequest authRequest = getAuthRequest(source);
        response.sendRedirect(authRequest.authorize(AuthStateUtils.createState()));
    }

    // oauth平台中配置的授权回调地址，以本项目为例，在创建gitee授权应用时的回调地址应为：http://localhost/dev-api/system/oauth/render/gitee/callback
    @RequestMapping("/{source}/callback")
    public Object login(@PathVariable("source") String source, AuthCallback callback) {
        AuthRequest authRequest = getAuthRequest(source);

        return authRequest.login(callback);
//        AuthResponse login = authRequest.login(callback);
//        JSONObject remoteData = JSONObject.parseObject(JSON.toJSONString(login));
//        Object gitEEData = remoteData.get("data");
//        JSONObject user = JSONObject.parseObject(JSON.toJSONString(gitEEData));
//        Object nickname = user.get("nickname");
//        Object email = user.get("email");
//        Object avatar = user.get("avatar");
//        HashMap<String, Object> userMap = new HashMap<>();
//        userMap.put("nickname",nickname);
//        userMap.put("email",email);
//        userMap.put("avatar",avatar);
//
//        // TODO
//
//        return userMap;
    }

    // 回收授权状态
    @RequestMapping("/revoke/{source}/{token}")
    public Object revokeAuth(@PathVariable("source") String source, @PathVariable("token") String token) throws IOException {
        AuthRequest authRequest = getAuthRequest(source);
        return authRequest.revoke(AuthToken.builder().accessToken(token).build());
    }

    // 刷新
    @RequestMapping("/refresh/{source}")
    public Object refreshAuth(@PathVariable("source") String source, String token) {
        AuthRequest authRequest = getAuthRequest(source);
        return authRequest.refresh(AuthToken.builder().refreshToken(token).build());
    }

    private AuthRequest getAuthRequest(String source) {
        AuthRequest authRequest = null;
        switch (source.toLowerCase()) {
            case "gitee":
                authRequest = new AuthGiteeRequest(AuthConfig.builder()
                        .clientId("f668b0167d544a9a0e2b5a02a0fca98a8f9a6d13de5fb04c945dc86f4760618f")
                        .clientSecret("235d7bd2f6a2028604dd4c722448477b0f32f200e0f71ba7b0297213c41ec52c")
                        .redirectUri("http://localhost:9999/oauth/gitee/callback")
                        .build());
                break;
            case "weibo":
                authRequest = new AuthWeiboRequest(AuthConfig.builder()
                        .clientId("")
                        .clientSecret("")
                        .redirectUri("http://dblog-web.zhyd.me/oauth/callback/weibo")
                        .scopes(Arrays.asList(
                                AuthWeiboScope.EMAIL.getScope(),
                                AuthWeiboScope.FRIENDSHIPS_GROUPS_READ.getScope(),
                                AuthWeiboScope.STATUSES_TO_ME_READ.getScope()
                        ))
                        .build());
                break;
        }
        if (null == authRequest) {
            throw new AuthException("未获取到有效的Auth配置");
        }
        return authRequest;
    }

//    private AuthRequest getAuthRequest() {
//        return new AuthGiteeRequest(AuthConfig.builder()
//                .clientId("f668b0167d544a9a0e2b5a02a0fca98a8f9a6d13de5fb04c945dc86f4760618f")
//                .clientSecret("07b226edbf73678eae1a6bf17149a7a1cde135a2568c164c762099711871f49c")
//                .redirectUri("http://localhost:9999/oauth/callback")
//                .build());
//    }
}
